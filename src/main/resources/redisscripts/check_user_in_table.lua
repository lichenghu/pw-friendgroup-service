local keyLen = #KEYS
if keyLen ~= 4 then
    return 0
end
local tableKeyPre = KEYS[1]
local tableId = KEYS[2]
local userId = KEYS[3]
local userGroupTableKeyPre = KEYS[4]

local userGroupTableKey = userGroupTableKeyPre .. userId
local userGroupTableId = redis.pcall("hget", userGroupTableKey, "tableId")
-- 不是同一个局
if (not userGroupTableId) or (tableId ~= userGroupTableId) then
    return -1
end

local tableKey = tableKeyPre .. tableId

local hAll = redis.pcall("hgetAll", tableKey)
if hAll then
    local kSitNum = "0"
    for i, v in pairs(hAll) do
        -- 获取userId
        if i % 2 == 0 then
            if v == userId then
                return tonumber(kSitNum)
            end
        else
            kSitNum = v
        end
    end
end
return -1