package com.tiantian.friendgroup.data.postgresql.design;

/**
 *
 */
public class SpingoRoomTable {
    public static final String TABLE_ROOM = "public.spingo_rooms";
    public static final String COLUMN_ROOM_ID = "room_id";
    public static final String COLUMN_ROOM_NAME = "room_name";
    public static final String COLUMN_ROOM_DESC = "room_desc";
    public static final String COLUMN_AVAILABLE = "available";
    public static final String COLUMN_RULE_ID = "rule_id";
    public static final String COLUMN_FEE = "fee";
    public static final String COLUMN_MAX_BONUS = "max_bonus";
    public static final String COLUMN_IS_LOCK = "is_lock";
    public static final String COLUMN_IS_DELETE = "is_delete";
}
