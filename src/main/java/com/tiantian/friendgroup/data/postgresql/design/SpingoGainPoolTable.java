package com.tiantian.friendgroup.data.postgresql.design;

/**
 *
 */
public class SpingoGainPoolTable {
    public static final String TABLE_GAIN_POOL = "public.spingo_gain_pool";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ROOM_ID = "room_id";
    public static final String COLUMN_TOTAL = "total";
    public static final String COLUMN_GAIN = "gain";
    public static final String COLUMN_REAL_GAIN = "real_gain";
    public static final String COLUMN_IN_TIMES = "in_times";
    public static final String COLUMN_UPDATE_DATE = "update_date";
    public static final String COLUMN_CREATE_DATE = "create_date";
    public static final String COLUMN_OUT_TIMES = "out_times";
}
