package com.tiantian.friendgroup.data.postgresql.design;

/**
 *
 */
public class SpingoChanceTable {
    public static final String TABLE_CHANCE = "public.spingo_chances";
    public static final String COLUMN_CHANCE_ID = "chance_id";
    public static final String COLUMN_ROOM_ID = "room_id";
    public static final String COLUMN_MONEY = "money";
    public static final String COLUMN_CHANCE = "chance";
}
