package com.tiantian.friendgroup.data.postgresql.design;

/**
 *
 */
public class SpingoRuleTable {
    public static final String TABLE_RULE = "public.spingo_rules";
    public static final String COLUMN_RULE_ID = "rule_id";
    public static final String COLUMN_BUY_IN = "buy_in";
    public static final String COLUMN_BIG_BLIND = "big_blind";
    public static final String COLUMN_SMALL_BLIND = "small_blind";
    public static final String COLUMN_UPGRADE_TICK = "upgrade_tick";
    public static final String COLUMN_PER_ADD = "per_add";
}
