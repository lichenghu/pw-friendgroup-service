package com.tiantian.friendgroup.handler;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MapReduceIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.util.JSON;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.fc.thrift.core.WinLoseLog;
import com.tiantian.friend.proxy_client.FriendIface;
import com.tiantian.friend.thrift.friend.Friend;
import com.tiantian.friendgroup.akka.ClusterClientManager;
import com.tiantian.friendgroup.akka.result.TableInfoResult;
import com.tiantian.friendgroup.akka.result.UserBetResult;
import com.tiantian.friendgroup.akka.result.UserInfoResult;
import com.tiantian.friendgroup.akka.result.UserStatusResult;
import com.tiantian.friendgroup.akka.user.*;
import com.tiantian.friendgroup.cache.LocalCache;
import com.tiantian.friendgroup.cache.MaintainInfo;
import com.tiantian.friendgroup.conts.RedisConts;
import com.tiantian.friendgroup.data.mongodb.MGDatabase;
import com.tiantian.friendgroup.data.redis.RedisUtil;
import com.tiantian.friendgroup.model.GameRecord;
import com.tiantian.friendgroup.thrift.event.*;
import com.tiantian.friendgroup.utils.RecordUtils;
import com.tiantian.message.proxy_client.MessageIface;
import com.tiantian.message.thrift.message.MessageInfo;
import com.tiantian.message.thrift.message.MessageType;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.printer.Printers;

import java.util.*;

/**
 *
 */
public class FriendGroupEventHandler implements FriendGroupEventService.Iface {
    static Logger LOG = LoggerFactory.getLogger(FriendGroupEventHandler.class);
    private static final String GROUPS_TABLE = "groups";
    private static final Map<Long, Long> PRICE_MAP;
    private static final Map<Long, Long> PRICE_BIGBLIND_MAP;
    private static final Map<Long, Long> PRICE_SMALLBLIND_MAP;
    private static final long MAX_BUY_IN_CNT = 5; // 朋友场最大买入次数
    private static final int WAIT_SECS = 15;
    // 房间最大人数
    private static final int GROUP_MAX_USERS = 9;
    private static String checkUserInTableScriptSha = null;
    private final static String CHECK_USER_IN_TABLE_SCRIPT_NAME_PATH = "redisscripts/check_user_in_table.lua";
    static {
        PRICE_MAP = new HashMap<>();
        PRICE_MAP.put((long) 200, (long) 10000);
        PRICE_MAP.put((long) 500, (long) 10000);
        PRICE_MAP.put((long) 1000, (long) 20000);
        PRICE_MAP.put((long) 2000, (long) 20000);
        PRICE_MAP.put((long) 5000, (long) 20000);
        PRICE_MAP.put((long) 10000, (long) 40000);
        PRICE_MAP.put((long) 20000, (long) 40000);
        PRICE_MAP.put((long) 50000, (long) 40000);
        PRICE_MAP.put((long) 100000, (long) 80000);
        PRICE_MAP.put((long) 1000000, (long) 160000);
        PRICE_BIGBLIND_MAP = new HashMap<>();
        PRICE_BIGBLIND_MAP.put((long) 200, (long) 2);
        PRICE_BIGBLIND_MAP.put((long) 500, (long) 4);
        PRICE_BIGBLIND_MAP.put((long) 1000, (long) 10);
        PRICE_BIGBLIND_MAP.put((long) 2000, (long) 20);
        PRICE_BIGBLIND_MAP.put((long) 5000, (long) 50);
        PRICE_BIGBLIND_MAP.put((long) 10000, (long) 100);
        PRICE_BIGBLIND_MAP.put((long) 20000, (long) 200);
        PRICE_BIGBLIND_MAP.put((long) 50000, (long) 400);
        PRICE_BIGBLIND_MAP.put((long) 100000, (long) 1000);
        PRICE_BIGBLIND_MAP.put((long) 1000000, (long) 10000);

        PRICE_SMALLBLIND_MAP = new HashMap<>();
        PRICE_SMALLBLIND_MAP.put((long) 200, (long) 1);
        PRICE_SMALLBLIND_MAP.put((long) 500, (long) 2);
        PRICE_SMALLBLIND_MAP.put((long) 1000, (long) 5);
        PRICE_SMALLBLIND_MAP.put((long) 2000, (long) 10);
        PRICE_SMALLBLIND_MAP.put((long) 5000, (long) 25);
        PRICE_SMALLBLIND_MAP.put((long) 10000, (long) 50);
        PRICE_SMALLBLIND_MAP.put((long) 20000, (long) 100);
        PRICE_SMALLBLIND_MAP.put((long) 50000, (long) 200);
        PRICE_SMALLBLIND_MAP.put((long) 100000, (long) 500);
        PRICE_SMALLBLIND_MAP.put((long) 1000000, (long) 5000);
    }

    @Override
    public String getServiceVersion() throws TException {
        return friendgroup_eventConstants.version;
    }

    @Override
    public void gameEvent(String cmd, String userId, String data) throws TException {
        String tableId = getUserGroupId(userId);
        if (StringUtils.isBlank(tableId)) {
            return;
        }
        switch (cmd) {
            case "fold" :
                TableUserFoldEvent tableUserFoldEvent = new TableUserFoldEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserFoldEvent);
                break;
            case "check" :
                TableUserCheckEvent tableUserCheckEvent = new TableUserCheckEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserCheckEvent);
                break;
            case "call" :
                TableUserCallEvent tableUserCallEvent = new TableUserCallEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserCallEvent);
                break;
            case "raise" :
                try {
                    String[] datas = data.split(",");
                    long raise = Long.parseLong(datas[0]);
                    String pwd = datas[1];
                    TableUserRaiseEvent tableUserRaiseEvent = new TableUserRaiseEvent(tableId, userId, raise, pwd);
                    ClusterClientManager.send(tableUserRaiseEvent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            case "allin" :
                TableUserAllinEvent tableUserAllinEvent = new TableUserAllinEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserAllinEvent);
                break;
            case "fast_raise" :
                try{
                    String[] datas = data.split(",");
                    TableUserFastRaiseEvent tableUserFastRaiseEvent = new TableUserFastRaiseEvent(tableId, userId, datas[0],  datas[1]);
                    ClusterClientManager.send(tableUserFastRaiseEvent);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                break;
            case "show_cards" :
                TableUserShowCardsEvent tableUserShowCardsEvent = new TableUserShowCardsEvent(tableId, userId, data);
                ClusterClientManager.send(tableUserShowCardsEvent);
                break;
            case "add_time" : // 增加时间
                TableUserAddTimesEvent tableUserAddTimesEvent = new TableUserAddTimesEvent(tableId, userId);
                ClusterClientManager.send(tableUserAddTimesEvent);
                break;
            case "emoji" :
                try {
                    String[] datas = data.split(",");
                    String toUserId = datas[0];
                    String emoji = datas[1];
                    TableUserEmojiEvent tableUserEmojiEvent = new TableUserEmojiEvent(tableId, userId, toUserId, emoji);
                    ClusterClientManager.send(tableUserEmojiEvent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public Group createGroup(String userId, long buyIn, double hours) throws TException {
        // 检测维护
        MaintainInfo maintainInfo = LocalCache.getAppStatus();
        if (maintainInfo != null) {
            long beginDate = maintainInfo.getBeginDate();
            if (beginDate <= System.currentTimeMillis()) {
                return new Group(-7, "服务器正在维护中不能组局",  null, userId, buyIn, 0, 0, 0, 0, 0, null, 0 ,0, 0);
            }
            long maintainHours = maintainInfo.getHours();
            return new Group (-7, "服务器正在维护中不能组局,维护时间" +maintainHours+"小时",  null, userId, buyIn, 0, 0, 0, 0, 0, null, 0 ,0, 0);
        }
        // 计算费用，然后扣费,房主只能同时创建一个
        Long price = PRICE_MAP.get(buyIn);
        if (price == null) {
            return new Group(Status.GROUP_NOT_EXISTS.getValue(), null,  null, userId, buyIn, 0, 0, 0, 0, 0, null, 0 ,0, 0);
        }

        List<Group> groupList = getUserCreateAndNotExpireGroups(userId);
        if (groupList != null && groupList.size() > 0) {
            return new Group(Status.HAS_NOT_EXPIRE_GROUP.getValue(), "已经创建了一个还没有结束的局，不能继续创建",  null, null, 0, 0, 0, 0, 0, 0,null, 0, 0, 0);
        }
        long fee = (long) ((double) price * hours);
        boolean ok = AccountIface.instance().iface().reduceUserFriendTicketsHours(userId, hours);
        if (!ok) {
            boolean isOk = AccountIface.instance().iface().reduceUserMoney(userId, fee);
            if (!isOk) {
                return new Group(Status.GOLD_NOT_ENOUGH.getValue(), "金币不足", null, userId, buyIn, 0, 0, 0, 0, 0, null, 0, 0, 0);
            }
        }

        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        Document document = new Document();
        document.put("masterId", userId);
        document.put("buyIn", buyIn);
        document.put("hours", hours);
        document.put("fee", fee);
        // 暂时不记录
        long startMills = System.currentTimeMillis() + 15 * 60000; // 延时15分钟
        document.put("startDate", startMills);
        long createDate = System.currentTimeMillis();
        document.put("createDate", createDate);
        document.put("forceOver", 0);
        document.put("bigBlind", PRICE_BIGBLIND_MAP.get(buyIn));
        document.put("smallBlind", PRICE_SMALLBLIND_MAP.get(buyIn));
        document.put("totalPlayCnt", 0L);
        List<Document> groupMembers = new ArrayList<>();
        // 添加成员
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        if (userDetail != null && StringUtils.isNotBlank(userDetail.getUserId())) {
            Document memDocument = new Document();
            memDocument.put("userId", userId);
            memDocument.put("isMaster", 1);
            memDocument.put("isJoin", 1);
            memDocument.put("nickName", userDetail.getNickName());
            memDocument.put("avatarUrl", userDetail.getAvatarUrl());
            memDocument.put("createDate", createDate);
            groupMembers.add(memDocument);
            document.put("groupMembers", groupMembers);
        }
        groupsCollection.insertOne(document);
        ObjectId groupId = document.getObjectId("_id");
        return new Group(Status.OK.getValue(), null,  groupId.toString(), userId, buyIn, 0, 0, 0, 0, 0, null, 0, 0, 0);
    }

    @Override
    public Group inviteMember(String groupId, String masterId, String userId) throws TException {
        // 添加成员
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        if (userDetail != null && StringUtils.isBlank(userDetail.getUserId())) {
            LOG.info("user not exists user id :" + userId);
            return new Group(Status.GROUP_MEMBERS_NOT_EXISTS.getValue(), "局成员信息不存在", null, null, 0, 0, 0, 0, 0, 0,null, 0, 0, 0);
        }
        Group group = getGroup(groupId);
        if (group == null || StringUtils.isBlank(group.getGroupId())) {
            LOG.info("group not exists group id :" + groupId);
            return new Group(Status.GROUP_NOT_EXISTS.getValue(), "局信息不存在", null, null, 0, 0, 0, 0, 0,0, null, 0, 0, 0);
        }
        // 不是房主
        if (!group.getMasterId().equalsIgnoreCase(masterId)) {
            return new Group(Status.GROUP_MEMBERS_NOT_EXISTS.getValue(), "局成员信息不存在", null, null, 0, 0, 0, 0, 0,0, null, 0, 0, 0);
        }
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            return new Group(Status.GROUP_EXPIRE.getValue(), "局已结束", null, null, 0, 0, 0, 0, 0,0, null, 0, 0, 0);
        }
        List<GroupMember> groupMemberList = group.getGroupMembers();
        for (GroupMember groupMember : groupMemberList) {
            if(groupMember.getUserId().equalsIgnoreCase(userId)) { // 玩家以存在
                return new Group(Status.OK.getValue(), null, null, null, 0, 0, 0, 0, 0,0, null, 0, 0, 0);
            }
        }
        // 添加成员
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        FindIterable<Document> limitIter = groupsCollection.find(new BasicDBObject("_id", new ObjectId(groupId)))
                .sort(new BasicDBObject()).limit(1);
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        List<Document> groupMembers = null;
        while (cursor.hasNext()) {
            doc = cursor.next();
            groupMembers = doc.get("groupMembers", List.class);
            if (groupMembers == null) {
                groupMembers = new ArrayList<>();
            }
            Document memDocument = new Document();
            memDocument.put("userId", userId);
            memDocument.put("isMaster", 0);
            memDocument.put("isJoin", 0);
            memDocument.put("nickName", userDetail.getNickName());
            memDocument.put("avatarUrl", userDetail.getAvatarUrl());
            memDocument.put("createDate", System.currentTimeMillis());
            groupMembers.add(memDocument);
        }
        if (groupMembers == null) {
            return new Group(Status.GROUP_NOT_EXISTS.getValue(), "局信息不存在", null, null, 0, 0, 0, 0, 0,0, null, 0, 0, 0);
        }
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("groupMembers", groupMembers);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        groupsCollection.updateOne(updateCondition, updateSetValue);

        UserDetail master = AccountIface.instance().iface().findUserDetailByUserId(masterId);
        if (master != null) {
            noticeUsers(userId, groupId, master.getNickName());
        }

        return new Group(Status.OK.getValue(), null, null, null, 0, 0, 0, 0, 0, 0,null, 0, 0, 0);
    }

    public void noticeUsers(String userId, String groupId, String nickName) {
        try {
            Map<String, String> contentMap = new HashMap<>();
            contentMap.put("groupId", groupId);
            contentMap.put("content", nickName + "邀请你加入牌局");
            MessageInfo messageInfo = new MessageInfo();
            messageInfo.setMessageEvent("group_invite");
            messageInfo.setMessageType(MessageType.GAME);
            messageInfo.setToUserId(userId);
            messageInfo.setMessageContent(com.alibaba.fastjson.JSON.toJSONString(contentMap));
            MessageIface.instance().iface().send(messageInfo);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Group getGroup(String groupId) throws TException {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        FindIterable<Document> limitIter = groupsCollection.find(new BasicDBObject("_id", new ObjectId(groupId)))
                .sort(new BasicDBObject()).limit(1);
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        while (cursor.hasNext()) {
            doc = cursor.next();
            ObjectId objectId = doc.getObjectId("_id");
            String masterId = doc.getString("masterId");
            long buyIn = doc.getLong("buyIn");
            double hours = doc.getDouble("hours");
            long startDate = doc.getLong("startDate");
            long fee = doc.getLong("fee");
            long createDate = doc.getLong("createDate");
            int forceOver = doc.getInteger("forceOver");
            long bigBlind =  doc.getLong("bigBlind");
            long smallBlind = doc.getLong("smallBlind");
            Long totalPlayCntObj = doc.getLong("totalPlayCnt");
            long totalPlayCnt = totalPlayCntObj == null ? 0 : totalPlayCntObj.longValue();
            List<Document> groupMembers = doc.get("groupMembers", List.class);
            List<GroupMember> groupMembersList = new ArrayList<>();
            if (groupMembers != null) {
                for (Document groupMemberDoc : groupMembers) {
                    String userId = groupMemberDoc.getString("userId");
                    Integer isMaster = groupMemberDoc.getInteger("isMaster", 0);
                    String nickName = groupMemberDoc.getString("nickName");
                    String avatarUrl = groupMemberDoc.getString("avatarUrl");
                    Integer isJoin = groupMemberDoc.getInteger("isJoin", 0);
                    GroupMember groupMember = new GroupMember(Status.OK.getValue(), null, userId, isMaster, nickName, avatarUrl, isJoin);
                    groupMembersList.add(groupMember);
                }
            }
            return new Group(Status.OK.getValue(), null, objectId.toString(), masterId, buyIn, hours, fee, startDate,
                    createDate, forceOver, groupMembersList, bigBlind, smallBlind, totalPlayCnt);
        }
        return new Group(Status.OK.getValue(), null,  null, null, 0, 0, 0, 0, 0,0, null, 0, 0, 0);
    }

    @Override
    public boolean updateGroupStartDate(String groupId) throws TException {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));
        updateCondition.put("startDate",  new BasicDBObject("$gt", System.currentTimeMillis()));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("startDate", System.currentTimeMillis());

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    @Override
    public List<Group> getUserCreateAndNotExpireGroups(String userId) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        long currentMill = System.currentTimeMillis();
        BasicDBObject dbObject = (BasicDBObject) JSON.parse("{$where : \"this.masterId == '" + userId + "' &&  " +
                " (this.forceOver != 1 && (this.startDate == 0 || this.startDate + this.hours * 3600000 >= " + currentMill + "))\"}");
        FindIterable<Document> limitIter = groupsCollection.find(dbObject)
                .sort(new BasicDBObject("createDate", -1));
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        List<Group> list = new ArrayList<>();
        while (cursor.hasNext()) {
            doc = cursor.next();
            ObjectId objectId = doc.getObjectId("_id");
            String masterId = doc.getString("masterId");
            long buyIn = doc.getLong("buyIn");
            double hours = doc.getDouble("hours");
            long startDate = doc.getLong("startDate");
            long fee = doc.getLong("fee");
            int forceOver = doc.getInteger("forceOver");
            long createDate = doc.getLong("createDate");
            long bigBlind =  doc.getLong("bigBlind");
            long smallBlind = doc.getLong("smallBlind");
            Long totalPlayCntObj = doc.getLong("totalPlayCnt");
            long totalPlayCnt = totalPlayCntObj == null ? 0 : totalPlayCntObj.longValue();
            List<Document> groupMembers = doc.get("groupMembers", List.class);
            List<GroupMember> groupMembersList = new ArrayList<>();
            if (groupMembers != null) {
                for (Document groupMemberDoc : groupMembers) {
                    String memUserId = groupMemberDoc.getString("userId");
                    Integer isMaster = groupMemberDoc.getInteger("isMaster");
                    String nickName = groupMemberDoc.getString("nickName");
                    String avatarUrl = groupMemberDoc.getString("avatarUrl");
                    Integer isJoin = groupMemberDoc.getInteger("isJoin", 0);
                    GroupMember groupMember = new GroupMember(Status.OK.getValue(), null, memUserId, isMaster, nickName, avatarUrl,isJoin);
                    groupMembersList.add(groupMember);
                }
            }
            Group group = new Group(Status.OK.getValue(), null, objectId.toString(), masterId, buyIn,
                    hours, fee, startDate, createDate, forceOver,groupMembersList, bigBlind, smallBlind, totalPlayCnt);

            list.add(group);
        }
        return list;
    }

    @Override
    public Group getUserCreateAndNotExpireGroup(String userId) throws TException {
        List<Group> list = getUserCreateAndNotExpireGroups(userId);
        if (list != null && list.size() > 0) {
            return list.get(0);
        }
        return new Group();
    }

    @Override
    public boolean forceOverGroup(String groupId, String masterId) throws TException {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));
        updateCondition.put("masterId", masterId);

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("forceOver", 1);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    @Override
    public Map<String, String> joinGroupGame(String groupId, String userId) throws TException {
        return joinGroup(groupId, userId, true);
    }

    @Override
    public Map<String, String> joinGroupGameWithoutInvite(String groupId, String userId) throws TException {
        return joinGroup(groupId, userId, false);
    }

    private Map<String, String> joinGroup(String groupId, String userId, boolean hasInvite)  throws TException{
        Map<String, String> map = new HashMap<>();
        Group group = getGroup(groupId);
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000
                        < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            map.put("status", "-2");
            map.put("errMsg", "该局已经结束");
            return map;
        }

        // 判断玩家是否已经在游戏里面
        long checkSitNum = checkUserInTable(groupId, userId);
        int sitNum = -1;
        if (checkSitNum > 0) { //已经存在房间里面
            sitNum = (int)checkSitNum;
        }
        else {
            // 检测玩家是否在其他房间里面
            String otherGroupId = RedisUtil.getFromMap(RedisConts.USER_GROUP_TABLE_KEY + userId, "tableId");
            if (StringUtils.isNotBlank(otherGroupId) && !otherGroupId.equalsIgnoreCase(groupId)) { // 还在其他房间里面
                exitGame(otherGroupId, userId); // 先退出
            }
        }

        List<GroupMember> groupMembers = group.getGroupMembers();
        if (groupMembers == null || groupMembers.size() == 0) {
            map.put("status", "-3");
            map.put("errMsg", "你没有被邀请到该局");
            return map;
        }
        boolean isInvite = false;
        for(GroupMember groupMember : groupMembers) {
            String inviteUserId = groupMember.getUserId();
            if (userId.equalsIgnoreCase(inviteUserId)) {
                isInvite = true;
                break;
            }
        }
        if (!isInvite) {
            if (hasInvite) {
                map.put("status", "-3");
                map.put("errMsg", "你没有被邀请到该局");
                return map;
            }
            else {
                // 自动加入到邀请列表中
                inviteMember(groupId, group.getMasterId(), userId);
            }
        }

        String masterName = "";
        UserDetail masterDetail = AccountIface.instance().iface().findUserDetailByUserId(group.getMasterId());
        if (masterDetail != null && StringUtils.isNotBlank(masterDetail.getNickName())) {
            masterName = masterDetail.getNickName();
        }
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        // 发送异步加入游戏消息
        ClusterClientManager.send(new TableUserJoinEvent(groupId, userId,userDetail.getAvatarUrl(),
                userDetail.getNickName(), userDetail.getGender(), group.getBuyIn(), group.getBigBlind(),
                group.getSmallBlind(), sitNum, group.getMasterId(), (group.getStartData() + (long)(group.getHours() * 3600000l))));
        map.put("status", "0");
        map.put("errMsg", "");
        map.put("group_id", groupId);
        map.put("sit_num", sitNum + "");
        map.put("max_people", GROUP_MAX_USERS + "");
        map.put("smbm", group.getSmallBlind() + "");
        map.put("bmbm", group.getBigBlind() + "");
        map.put("buy_in", group.getBuyIn() + "");
        map.put("hours", group.getHours() >= 1 ? (group.getHours() + "").substring(0, 1) : (group.getHours() + "").substring(0, 3));
        map.put("master_name", masterName);
        map.put("is_master", group.getMasterId().equalsIgnoreCase(userId) ? "1" : "0");
        map.put("wait_secs", WAIT_SECS + "");
        return map;
    }

    @Override
    public GroupResponse startGame(String groupId, String userId) throws TException {
        Group group = getGroup(groupId);
        if (group == null || !userId.equalsIgnoreCase(group.getMasterId())) {
            return new GroupResponse("-1", "局的信息不存在");
        }
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            return new GroupResponse("-2", "局已经结束");
        }
        long startData = group.getStartData();
        long endTimes = 0;
        long currentTimes = System.currentTimeMillis();
        if (currentTimes > startData) { // 应该已经开始了
            endTimes = startData + (long)(group.getHours() * 3600000.0d);
        }
        else { // 15分钟之内, 从当前时间开始计时
            endTimes = System.currentTimeMillis() + (long)(group.getHours() * 3600000.0d);
        }
        Object obj = ClusterClientManager.sendAndWait(new TableUserStartGameEvent(groupId, group.getBuyIn(), group.getSmallBlind(),
                group.getBigBlind(), endTimes, group.getMasterId(), group.getTotalPlayCnt()), 30);
        String result = (String) obj;
        if ("1".equalsIgnoreCase(result)) {
            //更新开始时间
            updateGroupStartDate(groupId);
            return new GroupResponse("0", "开始成功");
        } else if ("-1".equalsIgnoreCase(result)) {// 游戏已经开始过
            return new GroupResponse("-2", "局已经开始");
        } else if("-2".equalsIgnoreCase(result)){// 人数不齐
            return new GroupResponse("-3", "局人数不齐");
        }
        return new GroupResponse("-1", "局的信息不存在");
    }

    @Override
    public List<Map<String, String>> inviteFriends(String userId, String groupId, int beginCnt, int count) throws TException {
        List<Friend> list = FriendIface.instance().iface().getFriends(userId, beginCnt, count);
        List<Map<String, String>> resultList = new ArrayList<>();
        Map<String, String> memberUserIds = new HashMap<>();
        Group group = getGroup(groupId);
        if (group != null) {
            List<GroupMember> groupMemberList = group.getGroupMembers();
            if (groupMemberList != null) {
                for (GroupMember groupMember : groupMemberList) {
                    String memUserId = groupMember.getUserId();
                    memberUserIds.put(memUserId, "1");
                }
            }
        }
        if (list != null) {
            for (Friend friend : list) {
                String friendUserId = friend.getFriendUserId();
                Map<String, String> map = new HashMap<>();
                map.put("user_id", friend.getFriendUserId());
                map.put("nick_name", friend.getNickName() == null ? "" : friend.getNickName());
                map.put("avatar_url", friend.getAvatarUrl() == null ? "" : friend.getAvatarUrl());
                map.put("is_invite", memberUserIds.containsKey(friendUserId) ? "1" : "0");
                resultList.add(map);
            }
        }
        return resultList;
    }

    @Override
    public Map<String, String> sitDownFromTable(String userId) throws TException {
        Map<String, String> resultMap = new HashMap<>();
        String groupId = RedisUtil.getFromMap(RedisConts.USER_GROUP_TABLE_KEY + userId, "tableId");
        if (StringUtils.isBlank(groupId)) {
            resultMap.put("status", "-4");
            resultMap.put("errMsg", "牌局已经关闭");
            resultMap.put("group_id", "-1");
            resultMap.put("sit_num",  "0");
            resultMap.put("max_people",  "0");
            resultMap.put("smbm", "0");
            resultMap.put("bmbm", "0");
            resultMap.put("buy_in", "0");
            return resultMap;
        }
        Group group = getGroup(groupId);
        if (group == null) {
            return resultMap;
        }
        if(group.getForceOver() == 1) {
            resultMap.put("status", "-4");
            resultMap.put("errMsg", "牌局已经关闭");
            resultMap.put("group_id", groupId);
            resultMap.put("sit_num",  "0");
            resultMap.put("max_people",  "0");
            resultMap.put("smbm", "0");
            resultMap.put("bmbm", "0");
            resultMap.put("buy_in", "0");
            return resultMap;
        }

        if((group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            resultMap.put("status", "-5");
            resultMap.put("errMsg", "牌局已结束，不能坐下");
            resultMap.put("group_id", groupId);
            resultMap.put("sit_num",  "0");
            resultMap.put("max_people",  "0");
            resultMap.put("smbm", "0");
            resultMap.put("bmbm", "0");
            resultMap.put("buy_in", "0");
            return resultMap;
        }
        UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
        UserDetail master = AccountIface.instance().iface().findUserDetailByUserId(group.getMasterId());
        TableUserSitDownEvent.Response response = (TableUserSitDownEvent.Response) ClusterClientManager.sendAndWait(
                new TableUserSitDownEvent(groupId, userId, userDetail.getNickName(), group.getBuyIn(), group.getMasterId(),
                        group.getSmallBlind(), group.getBigBlind(), userDetail.getGender(), userDetail.getAvatarUrl(),
                        master.getNickName()), 30);
        int status = response.getStatus();
        String message = response.getMessage();
        int userSitNum = response.getUserSitNum();
        resultMap.put("status", status + "");
        resultMap.put("errMsg", message);
        resultMap.put("group_id", groupId);
        resultMap.put("sit_num",  (userSitNum > 0) ? userSitNum + "" : "0");
        resultMap.put("max_people",  GROUP_MAX_USERS + "");
        resultMap.put("smbm", group.getSmallBlind() + "");
        resultMap.put("bmbm", group.getBigBlind() + "");
        resultMap.put("buy_in", group.getBuyIn() + "");
        return resultMap;
    }

    private String getUserGroupId(String userId) {
        return RedisUtil.getFromMap(RedisConts.USER_GROUP_TABLE_KEY + userId, "tableId");
    }

    @Override
    public boolean standUpFromTable(String userId) throws TException {
        String groupId = getUserGroupId(userId);
        if (StringUtils.isBlank(groupId)) {
            return true;
        }
        return (boolean) ClusterClientManager.sendAndWait(new TableUserStandUpEvent(userId, groupId));
    }

    @Override
    public TableInfo getTableInfo(String userId) throws TException {
        String tableId = getUserGroupId(userId);
        if (StringUtils.isBlank(tableId)) {
            return new TableInfo();
        }
        TableInfo gTableInfo = new TableInfo();
        Group group = getGroup(tableId);
        if (group == null) {
           return gTableInfo;
        }

        TableInfoResult tableInfo =
                (TableInfoResult)ClusterClientManager.sendAndWait(new TableUserTableInfEvent(tableId, userId), 30);
        if (tableInfo != null) {
            gTableInfo.setTableId(tableId);
            gTableInfo.setInnerId(tableInfo.getInnerId());
            gTableInfo.setInnerCnt(tableInfo.getInnerCnt());
            List<UserInfo> userInfos = new ArrayList<>();
            if (tableInfo.getUsers() != null) {
                for (UserInfoResult userInfo : tableInfo.getUsers()) {
                    UserInfo $userInfo = new UserInfo();
                    $userInfo.setUserId(userInfo.getUserId());
                    $userInfo.setSitNum(userInfo.getSitNum());
                    $userInfo.setNickName(userInfo.getNickName());
                    $userInfo.setAvatarUrl(userInfo.getAvatarUrl());
                    $userInfo.setMoney(userInfo.getMoney());
                    $userInfo.setPlaying(userInfo.getPlaying());
                    $userInfo.setGender(userInfo.getGender());
                    $userInfo.setIsMaster(userInfo.getUserId().equals(group.getMasterId()) ? 1 : 0);
                    userInfos.add($userInfo);
                }
            }
            gTableInfo.setUsers(userInfos);
            List<UserStatus> userStatuses = new ArrayList<>();
            if (tableInfo.getUserStatus() != null) {
                for (UserStatusResult userStatus : tableInfo.getUserStatus()) {
                    UserStatus $userStatus = new UserStatus();
                    $userStatus.setSn(userStatus.getSn());
                    $userStatus.setHandCards(userStatus.getHandCards());
                    $userStatus.setStatus(userStatus.getStatus());
                    userStatuses.add($userStatus);
                }
            }
            gTableInfo.setUserStatus(userStatuses);
            gTableInfo.setCurBetSit(tableInfo.getCurBetSit());
            gTableInfo.setDeskCards(tableInfo.getDeskCards());
            gTableInfo.setHandCards(tableInfo.getHandCards());
            gTableInfo.setCardLevel(tableInfo.getCardLevel());
            gTableInfo.setLeftSecs(tableInfo.getLeftSecs());
            gTableInfo.setPool(tableInfo.getPool());
            List<UserBet> userBets = new ArrayList<>();
            if (tableInfo.getUserBets() != null) {
                for (UserBetResult userBet : tableInfo.getUserBets()) {
                    UserBet $userBets = new UserBet();
                    $userBets.setSn(userBet.getSn());
                    $userBets.setBet(userBet.getBet());
                    userBets.add($userBets);
                }
            }
            gTableInfo.setUserBets(userBets);
            gTableInfo.setPwd(tableInfo.getPwd());
            gTableInfo.setUserCanOps(tableInfo.getUserCanOps());
            gTableInfo.setButton(tableInfo.getButton());
            gTableInfo.setSmallBtn(tableInfo.getSmallBtn());
            gTableInfo.setBigBtn(tableInfo.getBigBtn());
            gTableInfo.setSmallBtnMoney(tableInfo.getSmallBtnMoney());
            gTableInfo.setBigBtnMoney(tableInfo.getBigBtnMoney());
            gTableInfo.setShowBegin(userId.equals(group.getMasterId()) ? tableInfo.getShowBegin() : -1);
            gTableInfo.setIsStopped(tableInfo.getIsStopped());
            gTableInfo.setGameStatus(tableInfo.getGameStatus());
        }
        return gTableInfo;
    }

    @Override
    public Group checkUserTableGroup(String userId) throws TException {
        String userTableKey = RedisConts.USER_GROUP_TABLE_KEY + userId;
        Map<String, String> map = RedisUtil.getMap(userTableKey);
        if (map != null) {
            String tableId = map.get("tableId");
            if (StringUtils.isNotBlank(tableId)) {
                String tableStatusKey = RedisConts.GROUP_TABLE_STATUS_KEY + tableId;
                boolean tableExists = RedisUtil.exists(tableStatusKey);
                String userOnlineKey = RedisConts.GROUP_TABLE_USER_ONLINE_KEY + tableId;
                boolean userOnlineExists = RedisUtil.hexists(userOnlineKey, userId);
                if (tableExists && userOnlineExists) {
                    Group group = getGroup(tableId);
                    if (group == null) {
                        return new Group();
                    }
                    UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
                    String nickName = "";
                    String avatarUrl = "";
                    String gender = "";
                    if (userDetail != null) {
                        nickName = userDetail.getNickName();
                        avatarUrl = userDetail.getAvatarUrl();
                        gender = userDetail.getGender();
                    }
                    ClusterClientManager.send(new TableUserRejoinEvent(tableId, userId, group.getMasterId(),
                            nickName, avatarUrl, gender));
                    return group;
                }
            }
        }
        return new Group();
    }

    @Override
    public GroupResponse closeGame(String groupId, String userId) throws TException {
        Group group = getGroup(groupId);
        if (group == null) {
            return new GroupResponse("-1", "局不存在");
        }
        if (!group.getMasterId().equalsIgnoreCase(userId)) {
            return new GroupResponse("-2", "当前玩家不是房主");
        }
        String ret = (String) ClusterClientManager.sendAndWait(new TableUserCloseGameEvent(groupId));
        if ("0".equalsIgnoreCase(ret)) {
            forceOverGroup(groupId, userId);
            return new GroupResponse("0", "关闭成功");
        }
        return new GroupResponse(ret, "关闭失败");
    }

    @Override
    public Map<String, Long> getGameTimes(String groupId) throws TException {
        Map<String, Long> map = new HashMap<>();

        Group group = getGroup(groupId);
        long startData = group.getStartData();
        long nowData = System.currentTimeMillis();
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            map.put("already_secs", (long) Math.min(group.getHours() * 3600.0d, Math.max(0.0d, (double)(nowData - startData) / 1000.0d)));
            map.put("left_secs", 0L);
            return map;
        }

        if (startData > nowData) { // 游戏未开始
            map.put("already_secs", 0L);
            map.put("left_secs", (long)(group.getHours() * 3600.0d));
        } else {
            long alreadySecs = (nowData - startData) / 1000; // 已经进行的时间
            long leftSecs = (long)(group.getHours() * 3600.0d) - alreadySecs; // 剩余的时间
            map.put("already_secs", alreadySecs);
            map.put("left_secs", leftSecs);
        }
        return map;
    }

    @Override
    public GroupLogDetail getGroupDetail(String groupId) throws TException {
        GroupLogDetail groupLogDetail = new GroupLogDetail();
        Group group = getGroup(groupId);
        if (group == null) {
            return  groupLogDetail;
        }
        Map<String, Long> map = getGameTimes(groupId);
        groupLogDetail.setAlreadySecs(map.get("already_secs"));
        groupLogDetail.setLeftSecs(map.get("left_secs"));
        List<WinLoseLog> winLoseLogs = FriendCoreIface.instance().iface().getGroupWinLoseLogs(groupId);
        List<GroupWinLoseLog> groupWinLoseLogs = new ArrayList<>();
        long createDate = 0;
        if (winLoseLogs != null && winLoseLogs.size() > 0) {
            for (WinLoseLog winLoseLog : winLoseLogs) {
                GroupWinLoseLog groupWinLoseLog = new GroupWinLoseLog(winLoseLog.getLogId(), winLoseLog.getGroupId(),
                        winLoseLog.getMasterNickName(), winLoseLog.getUserId(), winLoseLog.getNickName(), winLoseLog.getWin(),
                        winLoseLog.getPlayCnt(), winLoseLog.getLeftChips(), winLoseLog.getBuyInCnt(), winLoseLog.getAppStatus(),
                        winLoseLog.getCreateDate());
                if (createDate == 0) {
                    createDate = groupWinLoseLog.getCreateDate();
                }
                groupWinLoseLogs.add(groupWinLoseLog);
            }
        }
        groupLogDetail.setGroupWinLoseLogList(groupWinLoseLogs);

        groupLogDetail.setHours(group.getHours() >= 1 ? (group.getHours() + "").substring(0, 1) :
                (group.getHours() + "").substring(0, 3));
        groupLogDetail.setDate(DateFormatUtils.format(new Date(createDate), "yyyy-MM-dd HH:mm:ss"));
        groupLogDetail.setTotalPlayCnt(group.getTotalPlayCnt());
        return groupLogDetail;
    }


    @Override
    public Map<String, String> userHasJoinGroup(String userId, String groupId) throws TException {
        Map<String, String> map = new HashMap<>();
        Group group = getGroup(groupId);
        if(group == null || group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            map.put("status", "-2");
            map.put("errMsg", "该局已经结束");
            return map;
        }

        if (StringUtils.isNotBlank(groupId)) {
            String userTableKey = RedisConts.USER_GROUP_TABLE_KEY + userId;
            // Map<String, String> tableMap = RedisUtil.getMap(userTableKey);
            boolean isExists = RedisUtil.exists(userTableKey);
            //在游戏中
            if (isExists) {
                map.put("status", "0"); // 在房间
                map.put("errMsg", "");
                return map;
            }
        }
        map.put("status", "-1");
        map.put("errMsg", "已不在局中");
        return map;
    }

    @Override
    public Map<String, Long> getBuyInInfo(String userId, String groupId) throws TException {
        Map<String, Long> map = new HashMap<>();
        Group group = getGroup(groupId);
        long buyIn = 0;
        if (group != null) {
            buyIn = group.getBuyIn();
        }
        map.put("cnt", MAX_BUY_IN_CNT);
        map.put("buy_in", buyIn);
        String userChips = RedisUtil.getFromMap(RedisConts.USER_GROUP_GMAE_KEY + userId, groupId);
        JSONObject jsonObject;
        if (userChips != null) {
            jsonObject = com.alibaba.fastjson.JSON.parseObject(userChips);
            String chips = jsonObject.getString("chips");
            if (StringUtils.isNotBlank(chips)) {
                long leftChips = Long.parseLong(chips);
                if (leftChips > 0) {
                    if (group != null) {
                        long cnt =  MAX_BUY_IN_CNT - (long) Math.ceil((double)Math.max(leftChips, buyIn)/(double)buyIn);
                        map.put("cnt", Math.max(0, cnt));
                        map.put("buy_in", buyIn);
                    }
                }
            }
        }
        return map;
    }

    @Override
    public Map<String, Long> userBuyIn(String userId, String groupId, long userCnt) throws TException {
        Map<String, Long> map = getBuyInInfo(userId, groupId);
        Map<String, Long> retMap = new HashMap<>();
        retMap.put("cnt", 0l);
        retMap.put("buy_in", 0l);
        Group group = getGroup(groupId);
        if(group.getForceOver() == 1 ||
                (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
            LOG.info("group expire group id :" + groupId);
            return retMap;
        }
        List<GroupMember> groupMembers = group.getGroupMembers();
        if (groupMembers == null || groupMembers.size() == 0) {
            return retMap;
        }
        boolean isInvite = false;
        for(GroupMember groupMember : groupMembers) {
            String inviteUserId = groupMember.getUserId();
            if (userId.equalsIgnoreCase(inviteUserId)) {
                isInvite = true;
                break;
            }
        }
        if (!isInvite) { // 没有被邀请过
            return retMap;
        }

        long cnt = map.get("cnt");
        long buyIn = map.get("buy_in");
        if (userCnt <= 0) {
            return retMap;
        }
        userCnt = Math.min(cnt, userCnt);
        String result = (String) ClusterClientManager.sendAndWait(new TableUserBuyInEvent(groupId, userId, userCnt));
        if ("0".equals(result)) {
            retMap.put("cnt", userCnt);
            retMap.put("buy_in", buyIn);
        }
        return retMap;
    }

    @Override
    public Map<String, String> checkUserTableGroupMap(String userId) throws TException {
        Map<String, String> map = new HashMap<>();
        String userTableKey = RedisConts.USER_GROUP_TABLE_KEY + userId;
        Map<String, String> tableMap = RedisUtil.getMap(userTableKey);
        if (tableMap != null) {
            String tableId = tableMap.get("tableId");
            if (StringUtils.isNotBlank(tableId)) {

                Group group = getGroup(tableId);
                if(group == null || group.getForceOver() == 1 ||
                        (group.getStartData() != 0 && group.getStartData() + group.getHours() * 3600000 < System.currentTimeMillis())) {
                    LOG.info("checkUserTable group expire group id :" + tableId);
                    return map;
                }


                String tableStatusKey = RedisConts.GROUP_TABLE_STATUS_KEY + tableId;
                boolean tableExists = RedisUtil.exists(tableStatusKey);
                String userOnlineKey = RedisConts.GROUP_TABLE_USER_ONLINE_KEY + tableId;
                boolean userOnlineExists = RedisUtil.hexists(userOnlineKey, userId);
                if (tableExists && userOnlineExists) {
                    long checkSitNum = checkUserInTable(group.getGroupId(), userId);
                    int sitNum = -1;
                    if (checkSitNum > 0) { //已经存在房间里面
                        sitNum = (int)checkSitNum;
                    }
                    // 产生事件重新进入房间
                    UserDetail userDetail = AccountIface.instance().iface().findUserDetailByUserId(userId);
                    String nickName = "";
                    String avatarUrl = "";
                    String gender = "";
                    if (userDetail != null) {
                        nickName = userDetail.getNickName();
                        avatarUrl = userDetail.getAvatarUrl();
                        gender = userDetail.getGender();
                    }
                    ClusterClientManager.send(new TableUserRejoinEvent(tableId, userId, group.getMasterId(),
                            nickName, avatarUrl, gender));
                    map.put("status", "0");
                    map.put("errMsg", "");
                    map.put("group_id", group.getGroupId());
                    map.put("sit_num", sitNum + "");
                    map.put("max_people", GROUP_MAX_USERS + "");
                    map.put("smbm", group.getSmallBlind() + "");
                    map.put("bmbm", group.getBigBlind() + "");
                    map.put("buy_in", group.getBuyIn() + "");
                    map.put("hours", group.getHours() >= 1 ? (group.getHours() + "").substring(0, 1) : (group.getHours() + "").substring(0, 3));
                    String masterName = "";
                    UserDetail masterDetail = AccountIface.instance().iface().findUserDetailByUserId(group.getMasterId());
                    if (masterDetail != null && StringUtils.isNotBlank(masterDetail.getNickName())) {
                        masterName = masterDetail.getNickName();
                    }
                    map.put("master_name", masterName);
                    map.put("is_master", group.getMasterId().equalsIgnoreCase(userId) ? "1" : "0");
                    map.put("wait_secs", WAIT_SECS + "");
                }
            }
        }
        return map;
    }

    @Override
    public boolean exitGame(String groupId, String userId) throws TException {
        if (StringUtils.isBlank(groupId)) {
            groupId = getUserGroupId(userId);
        }
        try {
            Object ret = ClusterClientManager.sendAndWait(new TableUserExitEvent(userId, groupId), 20);
            return (boolean) ret;
        } catch (Exception e ) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public List<Group> getUserReferGroups(String userId) throws TException {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        long currentMill = System.currentTimeMillis();
        String mapFunc = "function () {" +
                "if (!this || this == undefined) {return;}" +
                "var flag = (this.forceOver != 1 && (this.startDate == 0 || this.startDate + this.hours * 3600000 >= " + currentMill + "));" +
                "if (!flag) { " +
                "    return;" +
                "} "+
                "for (var val in this.groupMembers){" +
                "    if (this.groupMembers[val].userId == '" + userId + "') {" +
                "         emit(this._id, this);return; " +
                "}}} ";
        String reduceFunc = "function(key, values) {" +
                "              return values;" +
                "        }";

        MapReduceIterable<Document> out = groupsCollection.mapReduce(mapFunc, reduceFunc);

        MongoCursor<Document> cursor = out.iterator();
        Document document = null;
        List<Group> list = new ArrayList<>();
        while (cursor.hasNext()) {
            document = cursor.next();
            ObjectId objectId = document.getObjectId("_id");
            Document doc =  (Document)document.get("value");
            String masterId = doc.getString("masterId");
            long buyIn = doc.getLong("buyIn");
            double hours = doc.getDouble("hours");
            long startDate = doc.getLong("startDate");
            long fee = doc.getLong("fee");
            int forceOver = doc.getInteger("forceOver");
            long createDate = doc.getLong("createDate");
            long bigBlind = doc.getLong("bigBlind");
            long smallBlind = doc.getLong("smallBlind");
            Long totalPlayCntObj = doc.getLong("totalPlayCnt");
            long totalPlayCnt = totalPlayCntObj == null ? 0 : totalPlayCntObj.longValue();
            List<Document> groupMembers = doc.get("groupMembers", List.class);
            List<GroupMember> groupMembersList = new ArrayList<>();
            if (groupMembers != null) {
                for (Document groupMemberDoc : groupMembers) {
                    String memUserId = groupMemberDoc.getString("userId");
                    Integer isMaster = groupMemberDoc.getInteger("isMaster");
                    String nickName = groupMemberDoc.getString("nickName");
                    String avatarUrl = groupMemberDoc.getString("avatarUrl");
                    Integer isJoin = groupMemberDoc.getInteger("isJoin", 0);
                    if (isJoin == 0) {
                        continue;
                    }
                    GroupMember groupMember = new GroupMember(Status.OK.getValue(), null, memUserId, isMaster, nickName, avatarUrl,isJoin);
                    groupMembersList.add(groupMember);
                }
            }
            Group group = new Group(Status.OK.getValue(), null, objectId.toString(), masterId, buyIn,
                    hours, fee, startDate, createDate, forceOver, groupMembersList, bigBlind, smallBlind, totalPlayCnt);
            list.add(group);
        }
        return list;
    }

    @Override
    public String getGameRecord(String tableId, String userId, int cnt) throws TException {
        if (tableId != null) {
            if (cnt <= 0) {
                cnt = 1;
            }
            GameRecord gameRecord = RecordUtils.getRecordByIndex(tableId, cnt);
            if (gameRecord == null) {
                return "";
            }
            gameRecord.generateProgresses(userId);
            return Printers.printString(Printers.prettyPrinterProtocol(), gameRecord.getRecord());
        }
        return "";
    }

    public long checkUserInTable(String groupId, String userId) {
        if (checkUserInTableScriptSha == null) {
            checkUserInTableScriptSha = RedisUtil.loadScript(CHECK_USER_IN_TABLE_SCRIPT_NAME_PATH);
        }
        List<String> keys = new ArrayList<>();
        keys.add(RedisConts.GROUP_TABLE_USER_KEY);
        keys.add(groupId);
        keys.add(userId);
        keys.add(RedisConts.USER_GROUP_TABLE_KEY);
        // 不需要vals 直接使用keys代替
        Object result = RedisUtil.execScript(checkUserInTableScriptSha, keys, keys);
        return (long) result;
    }

}
