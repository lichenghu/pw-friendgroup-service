package com.tiantian.friendgroup.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.friendgroup.akka.ClusterClientManager;
import com.tiantian.friendgroup.akka.event.TableTaskEvent;
import com.tiantian.friendgroup.conts.GameConstants;
import com.tiantian.friendgroup.thrift.task.FriendGroupTaskService;
import com.tiantian.friendgroup.thrift.task.friendgroup_taskConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

/**
 *
 */
public class FriendGroupTaskHandler implements  FriendGroupTaskService.Iface{
    @Override
    public String getServiceVersion() throws TException {
        return friendgroup_taskConstants.version;
    }

    @Override
    public boolean execTask(String taskStr) throws TException {
        try {
            JSONObject jsonObject = JSON.parseObject(taskStr);
            String taskEvent = jsonObject.getString(GameConstants.TASK_EVENT);
            if (StringUtils.isNotBlank(taskEvent)) {
                TableTaskEvent event = new TableTaskEvent();
                event.setEvent(taskEvent);
                event.setParams(jsonObject);
                ClusterClientManager.send(event);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
