package com.tiantian.friendgroup.akka.event;

import java.io.Serializable;

/**
 *
 */
public interface Event extends Serializable {
    String event();
}
