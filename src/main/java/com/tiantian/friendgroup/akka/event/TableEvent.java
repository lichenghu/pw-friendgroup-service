package com.tiantian.friendgroup.akka.event;

/**
 *
 */
public interface TableEvent extends Event {
    String tableId();
}
