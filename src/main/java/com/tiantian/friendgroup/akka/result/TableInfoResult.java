package com.tiantian.friendgroup.akka.result;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public class TableInfoResult implements Serializable {
    private List<UserInfoResult> users;
    private List<UserStatusResult>  userStatus;
    private String deskCards;
    private List<UserBetResult> userBets;
    private String innerId;
    private int curBetSit;
    private String handCards;
    private String pool;
    private long leftSecs;
    private long totalSecs;
    private String innerCnt;
    private String userCanOps;
    private String userWaitStatus;
    private int button;
    private int smallBtn;
    private int bigBtn;
    private int smallBtnMoney;
    private int bigBtnMoney;
    private int showBegin;
    private int isStopped;
    private String gameStatus;
    private String pwd;
    private String cardLevel;

    public List<UserInfoResult> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfoResult> users) {
        this.users = users;
    }

    public List<UserStatusResult> getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(List<UserStatusResult> userStatus) {
        this.userStatus = userStatus;
    }

    public String getDeskCards() {
        return deskCards;
    }

    public void setDeskCards(String deskCards) {
        this.deskCards = deskCards;
    }

    public List<UserBetResult> getUserBets() {
        return userBets;
    }

    public void setUserBets(List<UserBetResult> userBets) {
        this.userBets = userBets;
    }

    public String getInnerId() {
        return innerId;
    }

    public void setInnerId(String innerId) {
        this.innerId = innerId;
    }

    public int getCurBetSit() {
        return curBetSit;
    }

    public void setCurBetSit(int curBetSit) {
        this.curBetSit = curBetSit;
    }

    public String getHandCards() {
        return handCards;
    }

    public void setHandCards(String handCards) {
        this.handCards = handCards;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public long getLeftSecs() {
        return leftSecs;
    }

    public void setLeftSecs(long leftSecs) {
        this.leftSecs = leftSecs;
    }

    public String getInnerCnt() {
        return innerCnt;
    }

    public void setInnerCnt(String innerCnt) {
        this.innerCnt = innerCnt;
    }

    public String getUserCanOps() {
        return userCanOps;
    }

    public void setUserCanOps(String userCanOps) {
        this.userCanOps = userCanOps;
    }

    public int getButton() {
        return button;
    }

    public void setButton(int button) {
        this.button = button;
    }

    public int getSmallBtn() {
        return smallBtn;
    }

    public void setSmallBtn(int smallBtn) {
        this.smallBtn = smallBtn;
    }

    public int getBigBtn() {
        return bigBtn;
    }

    public void setBigBtn(int bigBtn) {
        this.bigBtn = bigBtn;
    }

    public int getSmallBtnMoney() {
        return smallBtnMoney;
    }

    public void setSmallBtnMoney(int smallBtnMoney) {
        this.smallBtnMoney = smallBtnMoney;
    }

    public int getBigBtnMoney() {
        return bigBtnMoney;
    }

    public void setBigBtnMoney(int bigBtnMoney) {
        this.bigBtnMoney = bigBtnMoney;
    }

    public int getShowBegin() {
        return showBegin;
    }

    public void setShowBegin(int showBegin) {
        this.showBegin = showBegin;
    }

    public int getIsStopped() {
        return isStopped;
    }

    public void setIsStopped(int isStopped) {
        this.isStopped = isStopped;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCardLevel() {
        return cardLevel;
    }

    public void setCardLevel(String cardLevel) {
        this.cardLevel = cardLevel;
    }

    public long getTotalSecs() {
        return totalSecs;
    }

    public void setTotalSecs(long totalSecs) {
        this.totalSecs = totalSecs;
    }

    public String getUserWaitStatus() {
        return userWaitStatus;
    }

    public void setUserWaitStatus(String userWaitStatus) {
        this.userWaitStatus = userWaitStatus;
    }


}
