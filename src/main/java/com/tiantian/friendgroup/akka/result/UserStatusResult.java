package com.tiantian.friendgroup.akka.result;

import java.io.Serializable;

/**
 *
 */
public class UserStatusResult implements Serializable {
    private int sn;
    private String handCards;
    private String status;

    public int getSn() {
        return sn;
    }

    public void setSn(int sn) {
        this.sn = sn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHandCards() {
        return handCards;
    }

    public void setHandCards(String handCards) {
        this.handCards = handCards;
    }
}
