package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserRejoinEvent extends TableUserEvent {
    private String tableId;
    private String userId ;
    private String masterId ;
    private String nickName ;
    private String avatarUrl ;
    private String gender;

    public TableUserRejoinEvent() {
    }

    public TableUserRejoinEvent(String tableId, String userId, String masterId, String nickName, String avatarUrl, String gender) {
        this.tableId = tableId;
        this.userId = userId;
        this.masterId = masterId;
        this.nickName = nickName;
        this.avatarUrl = avatarUrl;
        this.gender = gender;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userRejoin";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
