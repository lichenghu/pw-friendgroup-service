package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserExitEvent extends TableUserEvent {
    private String userId;
    private String tableId;

    public TableUserExitEvent(String userId, String tableId) {
        this.userId = userId;
        this.tableId = tableId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userExit";
    }

    public String getUserId() {
        return userId;
    }

    public String getTableId() {
        return tableId;
    }
}
