package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserRaiseEvent  extends TableUserEvent {
    private String tableId;
    private String userId;
    private long raise;
    private String pwd;

    public TableUserRaiseEvent() {
    }

    public TableUserRaiseEvent(String tableId, String userId, long raise, String pwd) {
        this.tableId = tableId;
        this.raise = raise;
        this.pwd = pwd;
        this.userId = userId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userRaise";
    }

    public String getTableId() {
        return tableId;
    }

    public long getRaise() {
        return raise;
    }

    public String getPwd() {
        return pwd;
    }

    public String getUserId() {
        return userId;
    }
}
