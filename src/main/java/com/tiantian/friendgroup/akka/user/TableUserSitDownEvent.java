package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

import java.io.Serializable;

/**
 *
 */
public class TableUserSitDownEvent extends TableUserEvent {
    private String groupId;
    private String userId;
    private String nickName;
    private long buyIn;
    private String masterId;
    private long smallBlind;
    private long bigBlind;
    private long totalBuyIn; // 用来判断买入总量 提示所有人带入信息
    private String avatarUrl;
    private String gender;
    private String masterNickName;


    public TableUserSitDownEvent(String groupId, String userId, String nickName, long buyIn, String masterId, long smallBlind,
                                 long bigBlind, String gender, String avatarUrl, String masterNickName) {
        this.groupId = groupId;
        this.userId = userId;
        this.buyIn = buyIn;
        this.masterId = masterId;
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
        this.nickName = nickName;
        this.gender = gender;
        this.avatarUrl = avatarUrl;
        this.masterNickName = masterNickName;
    }

    @Override
    public String tableId() {
        return groupId;
    }

    @Override
    public String event() {
        return "userSitDown";
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUserId() {
        return userId;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public String getMasterId() {
        return masterId;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public long getTotalBuyIn() {
        return totalBuyIn;
    }

    public String getNickName() {
        return nickName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getGender() {
        return gender;
    }

    public String getMasterNickName() {
        return masterNickName;
    }

    public static class Response implements Serializable {
        private int status;
        private String message;
        private int userSitNum;

        public Response(int status, String message, int userSitNum) {
            this.status = status;
            this.message = message;
            this.userSitNum = userSitNum;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getUserSitNum() {
            return userSitNum;
        }

        public void setUserSitNum(int userSitNum) {
            this.userSitNum = userSitNum;
        }
    }
}
