package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserBuyInEvent extends TableUserEvent {
    private String tableId;
    private String userId;
    private long buyInCnt;

    public TableUserBuyInEvent() {
    }

    public TableUserBuyInEvent(String tableId, String userId, long buyInCnt) {
        this.tableId = tableId;
        this.userId = userId;
        this.buyInCnt = buyInCnt;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userBuyIn";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getBuyInCnt() {
        return buyInCnt;
    }

    public void setBuyInCnt(long buyInCnt) {
        this.buyInCnt = buyInCnt;
    }
}
