package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserTableInfEvent extends TableUserEvent {
    private String tableId;
    private String userId;

    public TableUserTableInfEvent(String tableId, String userId) {
        this.tableId = tableId;
        this.userId = userId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "tableInfo";
    }

    public String getUserId() {
        return userId;
    }
}
