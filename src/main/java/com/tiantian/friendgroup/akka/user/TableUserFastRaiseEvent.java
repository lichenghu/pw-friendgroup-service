package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserFastRaiseEvent extends TableUserEvent {
    private String tableId;
    private String userId;
    private String type;
    private String pwd;

    public TableUserFastRaiseEvent() {

    }


    public TableUserFastRaiseEvent(String tableId, String userId, String type, String pwd) {
        this.tableId = tableId;
        this.userId = userId;
        this.type = type;
        this.pwd = pwd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userFastRaise";
    }
}
