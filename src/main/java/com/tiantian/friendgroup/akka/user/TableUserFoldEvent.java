package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserFoldEvent  extends TableUserEvent {
    private String tableId;
    private String userId;
    private String pwd;

    public TableUserFoldEvent() {
    }

    public TableUserFoldEvent(String tableId, String userId, String pwd) {
        this.tableId = tableId;
        this.userId = userId;
        this.pwd = pwd;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userFold";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
