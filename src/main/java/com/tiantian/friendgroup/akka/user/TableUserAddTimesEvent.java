package com.tiantian.friendgroup.akka.user;

import com.tiantian.friendgroup.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserAddTimesEvent extends TableUserEvent {
    private String groupId;
    private String userId;

    public TableUserAddTimesEvent(String groupId, String userId) {
        this.groupId = groupId;
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String tableId() {
        return groupId;
    }

    @Override
    public String event() {
        return "addTimes";
    }
}
