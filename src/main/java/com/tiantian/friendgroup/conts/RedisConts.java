package com.tiantian.friendgroup.conts;

/**
 *
 */
public interface RedisConts {
    // 局桌子玩家
    String GROUP_TABLE_USER_KEY = "group_table_user_key:";

    // 局玩家
    String GROUP_USER_KEY = "group_user_key:";

    // 局桌子在线玩家
    String  GROUP_TABLE_USER_ONLINE_KEY = "group_table_user_online_key:";

    // 局桌子坐下过的玩家
    String  GROUP_TABLE_USER_SIT_DOWN_KEY = "group_table_user_sit_down_key:";

    // 桌子上玩家
    String USER_GROUP_TABLE_KEY = "user_group_table_key:";

    String USER_GROUP_GMAE_KEY = "user_group_games_key:";

    String GROUP_TABLE_STATUS_KEY = "group_table_status:";
}
