package com.tiantian.friendgroup.model;

import us.bpsm.edn.Keyword;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class GameRecord {
    private String gameType;
    private String remark;
    private String tableId;
    private String innerId;
    private long startTime;
    private int dealerNumber;
    private int smallBlind;
    private int bigBlind;
    private int utgNumber;
    private int tableType;
    private List<RecordUser> recordUsers = new ArrayList<>();
    private List<Progress> progresses = new ArrayList<>();
    private Map<String, String> showCardsSits = new HashMap<>();
    private Map<Object,Object> record;


    public GameRecord() {
        record = new HashMap<>();
    }

    public static class Progress {
        private String opration;
        private String oprator;
        private String value;
        private long time;

        public static Progress create(String opration, String oprator,  String value,  long time) {
            Progress progress = new Progress();
            progress.setOpration(opration);
            progress.setOprator(oprator);
            progress.setValue(value);
            progress.setTime(time);
            return progress;
        }

        public String getOpration() {
            return opration;
        }

        public void setOpration(String opration) {
            this.opration = opration;
        }

        public String getOprator() {
            return oprator;
        }

        public void setOprator(String oprator) {
            this.oprator = oprator;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }
    }
    public static class RecordUser {
        private String id;
        private String nickName;
        private String avatarUrl;
        private String sitNumber;
        private long score;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public String getSitNumber() {
            return sitNumber;
        }

        public void setSitNumber(String sitNumber) {
            this.sitNumber = sitNumber;
        }

        public long getScore() {
            return score;
        }

        public void setScore(long score) {
            this.score = score;
        }
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
        record.put(Keyword.newKeyword("table_type"),this.tableType);
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
        record.put(Keyword.newKeyword("start_time"),this.startTime);
    }

    public int getDealerNumber() {
        return dealerNumber;
    }

    public void setDealerNumber(int dealerNumber) {
        this.dealerNumber = dealerNumber;
        record.put(Keyword.newKeyword("dealer_number"),this.dealerNumber);
    }

    public int getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(int smallBlind) {
        this.smallBlind = smallBlind;
        record.put(Keyword.newKeyword("small_blind"),this.smallBlind);
    }

    public int getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(int bigBlind) {
        this.bigBlind = bigBlind;
        record.put(Keyword.newKeyword("big_blind"),this.bigBlind);
    }

    public int getUtgNumber() {
        return utgNumber;
    }

    public void setUtgNumber(int utgNumber) {
        this.utgNumber = utgNumber;
        record.put(Keyword.newKeyword("utg_number"),this.utgNumber);
    }

    public int getTableType() {
        return tableType;
    }

    public void setTableType(int tableType) {
        this.tableType = tableType;
        record.put(Keyword.newKeyword("g_type"),this.gameType);
    }

    public List<RecordUser> getRecordUsers() {
        return recordUsers;
    }

    public void setRecordUsers(List<RecordUser> recordUsers) {
        this.recordUsers = recordUsers;
        List<Object> uedns = new ArrayList<>();
        for (RecordUser u : recordUsers) {
            Map<Object,Object> user_map = new HashMap();
            user_map.put(Keyword.newKeyword("id"),u.getId());
            user_map.put(Keyword.newKeyword("name"),u.getNickName());
            user_map.put(Keyword.newKeyword("avatar_url"),u.getAvatarUrl());
            user_map.put(Keyword.newKeyword("site_number"),u.getSitNumber());
            user_map.put(Keyword.newKeyword("score"),u.getScore());
            uedns.add(user_map);
        }
        record.put(Keyword.newKeyword("users"),uedns);
    }

    public List<Progress> getProgresses() {
        return progresses;
    }

    public void setProgresses(List<Progress> progresses) {
        this.progresses = progresses;
    }

    public void generateProgresses(String userId) {
        List<Object> pedns = new ArrayList<>();
        String selfSit = null;
        for (RecordUser recordUser : recordUsers) {
             if(recordUser.getId().equals(userId)) {
                 selfSit = recordUser.getSitNumber();
             }
        }
        for (Progress o : progresses) {
            Map<Object, Object> progress_map = new HashMap<>();
            if ("deal".equals(o.getOpration())) {
                String sit = o.getOprator();
                String cards = showCardsSits.get(sit);
                if (cards == null) {
                    cards = "";
                }
                if (selfSit != null && selfSit.equals(sit)) {
                    cards = o.getValue();
                }
                progress_map.put(Keyword.newKeyword("opration"), o.getOpration());
                progress_map.put(Keyword.newKeyword("oprator"), o.getOprator());
                progress_map.put(Keyword.newKeyword("value"), cards);
                progress_map.put(Keyword.newKeyword("time"), o.getTime());
            } else {
                progress_map.put(Keyword.newKeyword("opration"), o.getOpration());
                progress_map.put(Keyword.newKeyword("oprator"), o.getOprator());
                progress_map.put(Keyword.newKeyword("value"), o.getValue());
                progress_map.put(Keyword.newKeyword("time"), o.getTime());
            }
            pedns.add(progress_map);
        }
        record.put(Keyword.newKeyword("progresses"),pedns);
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getInnerId() {
        return innerId;
    }

    public void setInnerId(String innerId) {
        this.innerId = innerId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
        record.put(Keyword.newKeyword("remark"),this.remark);
    }

    public Map<Object, Object> getRecord() {
        return record;
    }

    public void setRecord(Map<Object, Object> record) {
        this.record = record;
    }

    public Map<String, String> getShowCardsSits() {
        return showCardsSits;
    }

    public void setShowCardsSits(Map<String, String> showCardsSits) {
        this.showCardsSits = showCardsSits;
    }
}
