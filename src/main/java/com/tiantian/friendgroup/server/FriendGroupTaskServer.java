package com.tiantian.friendgroup.server;

import com.tiantian.friendgroup.akka.ClusterClientManager;
import com.tiantian.friendgroup.data.mongodb.MGDatabase;
import com.tiantian.friendgroup.data.postgresql.PGDatabase;
import com.tiantian.friendgroup.handler.FriendGroupTaskHandler;
import com.tiantian.friendgroup.settings.FriendGroupTaskConfig;
import com.tiantian.friendgroup.settings.PGConfig;
import com.tiantian.friendgroup.thrift.task.FriendGroupTaskService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class FriendGroupTaskServer {
    private static Logger LOG = LoggerFactory.getLogger(FriendGroupTaskServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {

        ClusterClientManager.init((FriendGroupTaskConfig.getInstance().getPort() + 10) + "");

        FriendGroupTaskServer friendGroupTaskServer = new FriendGroupTaskServer();
        friendGroupTaskServer.init(args);
        friendGroupTaskServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            MGDatabase.getInstance().init();

            FriendGroupTaskHandler handler = new FriendGroupTaskHandler();
            TProcessor tProcessor = new  FriendGroupTaskService.Processor<FriendGroupTaskService.Iface>(handler);
            TServerSocket tss = new TServerSocket(FriendGroupTaskConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());
        }
        catch (Exception e) {
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", FriendGroupTaskConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
