package com.tiantian.friendgroup.server;

import com.tiantian.friendgroup.akka.ClusterClientManager;
import com.tiantian.friendgroup.data.mongodb.MGDatabase;
import com.tiantian.friendgroup.data.postgresql.PGDatabase;
import com.tiantian.friendgroup.handler.FriendGroupEventHandler;
import com.tiantian.friendgroup.settings.FriendGroupEventConfig;
import com.tiantian.friendgroup.settings.PGConfig;
import com.tiantian.friendgroup.thrift.event.FriendGroupEventService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class FriendGroupEventServer {
    private static Logger LOG = LoggerFactory.getLogger(FriendGroupEventServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {
        ClusterClientManager.init((FriendGroupEventConfig.getInstance().getPort() + 5) + "");

        FriendGroupEventServer friendGroupEventServer = new FriendGroupEventServer();
        friendGroupEventServer.init(args);
        friendGroupEventServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            PGDatabase.getInstance().init(
                    PGConfig.getInstance().getHost(),
                    PGConfig.getInstance().getPort(),
                    PGConfig.getInstance().getDb(),
                    PGConfig.getInstance().getUsername(),
                    PGConfig.getInstance().getPassword()
            );
            MGDatabase.getInstance().init();

            FriendGroupEventHandler handler = new FriendGroupEventHandler();

            TProcessor tProcessor = new FriendGroupEventService.Processor<FriendGroupEventService.Iface>(handler);
            TServerSocket tss = new TServerSocket(FriendGroupEventConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", FriendGroupEventConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
