package com.tiantian.friendgroup.cache;

/**
 * 维护信息
 */
public class MaintainInfo {
    private String appName; // 应用名称
    private long beginDate; // 开始时间
    private long hours; // 维护时间

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public long getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(long beginDate) {
        this.beginDate = beginDate;
    }

    public long getHours() {
        return hours;
    }

    public void setHours(long hours) {
        this.hours = hours;
    }
}
