package com.tiantian.core.proxy_client;

import com.tiantian.core.thrift.account.AccountService;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

/**
 *
 */
public class AccountIface {
    private static AccountService.Iface clientProxy;

    private static class AccountIfaceHolder {
        private final static AccountIface instance = new AccountIface();
    }

    private AccountIface() {
        ClassLoader classLoader = AccountService.Iface.class.getClassLoader();
        clientProxy = (AccountService.Iface) Proxy.newProxyInstance(classLoader,
                new Class[]{AccountService.Iface.class},
                (proxy, method, args) -> {
                    TSocket ttSocket = ClientPool.getInstance().borrowObject();
                    try {
                        TFastFramedTransport fastTransport = new TFastFramedTransport(ttSocket);
                        TCompactProtocol protocol = new TCompactProtocol(fastTransport);
                        AccountService.Client client = new AccountService.Client(protocol);
                        //设置成可以访问
                        method.setAccessible(true);
                        return method.invoke(client, args);
                    } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
                        if (e instanceof InvocationTargetException) {
                            Throwable throwable = ((InvocationTargetException) e).getTargetException();
                            throwable.printStackTrace();
                            throw throwable;
                        }
                        e.printStackTrace();
                        throw e;
                    } finally {
                        ClientPool.getInstance().returnObject(ttSocket);
                    }
                });
    }

    public static AccountIface instance() {
        return AccountIfaceHolder.instance;
    }

    public AccountService.Iface iface() {
        return clientProxy;
    }
}
