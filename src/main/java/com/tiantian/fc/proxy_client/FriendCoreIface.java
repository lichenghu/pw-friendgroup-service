package com.tiantian.fc.proxy_client;

import com.tiantian.fc.settings.FriendCoreConfig;
import com.tiantian.fc.thrift.core.FriendCoreService;
import com.tiantian.framework.thrift.client.IFace;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class FriendCoreIface extends IFace<FriendCoreService.Iface> {

    private static class FriendCoreIfaceHolder {
        private final static FriendCoreIface instance = new FriendCoreIface();
    }

    private FriendCoreIface() {
    }

    public static FriendCoreIface instance() {
        return FriendCoreIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new FriendCoreService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(FriendCoreConfig.getInstance().getHost(),
                FriendCoreConfig.getInstance().getPort());
    }

    @Override
    protected Class<FriendCoreService.Iface> faceClass() {
        return FriendCoreService.Iface.class;
    }
}
