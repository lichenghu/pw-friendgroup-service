package com.tiantian.friend.proxy_client;

import com.tiantian.friend.thrift.friend.FriendService;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

/**
 *
 */
public class FriendIface {
    private static FriendService.Iface clientProxy;

    private static class FriendIfaceHolder {
        private final static FriendIface instance = new FriendIface();
    }

    private FriendIface() {
        ClassLoader classLoader = FriendService.Iface.class.getClassLoader();
        clientProxy = (FriendService.Iface) Proxy.newProxyInstance(classLoader,
                new Class[]{FriendService.Iface.class},
                (proxy, method, args) -> {
                    TSocket ttSocket = ClientPool.getInstance().borrowObject();
                    try {
                        TFastFramedTransport fastTransport = new TFastFramedTransport(ttSocket);
                        TCompactProtocol protocol = new TCompactProtocol(fastTransport);
                        FriendService.Client client = new FriendService.Client(protocol);
                        //设置成可以访问
                        method.setAccessible(true);
                        return method.invoke(client, args);
                    } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
                        if (e instanceof InvocationTargetException) {
                            Throwable throwable = ((InvocationTargetException) e).getTargetException();
                            throwable.printStackTrace();
                            throw throwable;
                        }
                        e.printStackTrace();
                        throw e;
                    } finally {
                        ClientPool.getInstance().returnObject(ttSocket);
                    }
                });
    }

    public static FriendIface instance() {
        return FriendIfaceHolder.instance;
    }

    public FriendService.Iface iface() {
        return clientProxy;
    }
}
