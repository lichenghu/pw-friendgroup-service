package com.tiantian.message.proxy_client;

import com.tiantian.message.thrift.message.MessageService;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

/**
 *
 */
public class MessageIface {
    private static MessageService.Iface clientProxy;

    private static class MessageIfaceHolder {
        private final static MessageIface instance = new MessageIface();
    }

    private MessageIface() {
        ClassLoader classLoader = MessageService.Iface.class.getClassLoader();
        clientProxy = (MessageService.Iface) Proxy.newProxyInstance(classLoader,
                new Class[]{MessageService.Iface.class},
                (proxy, method, args) -> {
                    TSocket ttSocket = ClientPool.getInstance().borrowObject();
                    try {
                        TFastFramedTransport fastTransport = new TFastFramedTransport(ttSocket);
                        TCompactProtocol protocol = new TCompactProtocol(fastTransport);
                        MessageService.Client client = new MessageService.Client(protocol);
                        //设置成可以访问
                        method.setAccessible(true);
                        return method.invoke(client, args);
                    } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
                        if (e instanceof InvocationTargetException) {
                            Throwable throwable = ((InvocationTargetException) e).getTargetException();
                            throwable.printStackTrace();
                            throw throwable;
                        }
                        e.printStackTrace();
                        throw e;
                    } finally {
                        ClientPool.getInstance().returnObject(ttSocket);
                    }
                });
    }

    public static MessageIface instance() {
        return MessageIfaceHolder.instance;
    }

    public MessageService.Iface iface() {
        return clientProxy;
    }
}
