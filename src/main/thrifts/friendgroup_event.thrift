namespace java com.tiantian.friendgroup.thrift.event

const string version = '1.0.0'

enum Status{
    OK = 0,     //用户调用ok
    GROUP_MEMBERS_MAX = -1, // 群组人数达到最大
    GROUP_MEMBERS_NOT_EXISTS = -2, // 群组成员不存在
    GROUP_NOT_EXISTS = -3, //群组不存在
    GROUP_EXPIRE = -4, //群组不存在
    HAS_NOT_EXPIRE_GROUP = -5, //已经创建了一个没有结束的局
    GOLD_NOT_ENOUGH = -6,
    WARNING_SQL_NULL_QUERY = 500,
    ERROR_SQL_SYNTAX = 501,
    ERROR_SQL_FETCH_RESULT = 502,
    ERROR_SQL_INSERT_RESULT = 503,
    ERROR_SQL_UPDATE_RESULT = 504,
    ERROR_SQL_DELETE_RESULT = 505,
    ERROR_INIT = 999
}

struct GroupMember {
    1:i32       status,
    2:string    errMsg,
    3:string    userId,
    4:i32       isMaster,
    5:string    nickName,
    6:string    avatarUrl,
    7:i32       isJoin
}

struct Group {
    1:i32       status,
    2:string    errMsg,
    3:string    groupId,
    4:string    masterId,
    5:i64       buyIn,
    6:double       hours,
    7:i64       fee,
    8:i64       startData,
    9:i64       createDate,
    10:i32      forceOver,
    11:list<GroupMember> groupMembers,
    12:i64 bigBlind,
    13:i64  smallBlind,
    14:i64  totalPlayCnt
}

struct GroupResponse {
    1:string    status,
    2:string    errMsg
}

struct UserStatus {
  1:i32 sn,
  2:string status
  3:string handCards
}

struct UserBet {
 1:i32 sn,
 2:i64 bet
}

struct UserInfo {
  1:string userId,
  2:i32 sitNum,
  3:string  avatarUrl,
  4:string  nickName,
  5:i64  money,
  6:i32  playing,
  7:string gender,
  8:i32 isMaster
}

struct TableInfo {
    1:list<UserInfo>    users,
    2:list<UserStatus>  userStatus,
    3:string            deskCards,
    4:list<UserBet>     userBets,
    5:string            innerId,
    6:i32               curBetSit,
    7:string            handCards,
    8:string            pool,
    9:i64               leftSecs,
    10:string           innerCnt,
    11:string           userCanOps,
    12:i32              button,
    13:i32              smallBtn,
    14:i32              bigBtn,
    15:i32              smallBtnMoney,
    16:i32              bigBtnMoney,
    17:i32              showBegin,
    18:i32              isStopped,
    19:string           gameStatus,
    20:string           pwd,
    21:string           cardLevel,
    22:string           tableId
}

struct GroupWinLoseLog {
   1:string logId,
   2:string groupId,
   3:string masterNickName,
   4:string userId,
   5:string nickName,
   6:i64  win,
   7:i32  playCnt,
   8:i64  leftChips,
   9:i32  buyInCnt,
   10:i32 appStatus,
   11:i64 createDate
}

struct GroupLogDetail {
    1:i64 alreadySecs,
    2:i64 leftSecs,
    3:list<GroupWinLoseLog> groupWinLoseLogList,
    4:string hours,
    5:i64 totalPlayCnt,
    6:string date
}

service FriendGroupEventService {
    string getServiceVersion()
    void gameEvent(1:string cmd, 2:string userId, 3:string data),
    Group createGroup(1:string userId, 2:i64 buyIn, 3:double hours),
    Group inviteMember(1:string groupId, 2:string masterId, 3:string userId),
    Group getGroup(1:string groupId),
    bool updateGroupStartDate(1:string groupId),
    list<Group> getUserCreateAndNotExpireGroups(1:string userId),
    Group getUserCreateAndNotExpireGroup(1:string userId),
    bool forceOverGroup(1:string groupId, 2:string masterId),
    map<string, string> joinGroupGame(1:string groupId, 2:string userId),
    map<string, string> joinGroupGameWithoutInvite(1:string groupId, 2:string userId),
    GroupResponse startGame(1:string groupId, 2:string userId),
    bool exitGame(1:string groupId, 2:string userId),
    list<map<string, string>> inviteFriends(1:string userId, 2:string groupId, 3:i32 beginCnt, 4:i32 count),
    map<string, string> sitDownFromTable(1:string userId)
    bool standUpFromTable(1:string userId),
    TableInfo getTableInfo(1:string userId),
    Group checkUserTableGroup(1:string userId),
    GroupResponse closeGame(1:string groupId, 2:string userId),
    map<string, i64> getGameTimes(1:string groupId),
    GroupLogDetail getGroupDetail(1:string groupId),
    map<string, string> userHasJoinGroup(1:string userId, 2:string groupId),
    map<string, i64> getBuyInInfo(1:string userId, 2:string groupId),
    map<string, i64> userBuyIn(1:string userId, 2:string groupId, 3:i64 userCnt)
    map<string, string> checkUserTableGroupMap(1:string userId)
    list<Group> getUserReferGroups(1:string userId),
    string getGameRecord(1:string tableId, 2:string userId, 3:i32 cnt)
}