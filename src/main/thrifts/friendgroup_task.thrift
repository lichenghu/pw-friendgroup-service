namespace java com.tiantian.friendgroup.thrift.task

const string version = '1.0.0'

service FriendGroupTaskService {
  string getServiceVersion(),
  bool execTask(1:string taskStr)
}